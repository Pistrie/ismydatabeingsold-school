import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { ValidatorService } from './validator.service';

@Directive({
  selector: '[appEmailValidator]',
  providers: [{ provide: NG_VALIDATORS, useExisting: EmailValidatorDirective, multi: true }],
})
export class EmailValidatorDirective implements Validator {

  constructor(private validator: ValidatorService) {
  }

  validate(control: AbstractControl): { [key: string]: any } | null {
    return this.validator.emailAddressValidator()(control);
  }
}
