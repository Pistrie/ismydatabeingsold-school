import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { Company } from '../../company/company.model';
import { CompanyService } from '../../company/company.service';
import { LicenseType, Product } from '../product.model';
import { ProductService } from '../product.service';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss'],
})
export class ProductEditComponent implements OnInit {
  subscription: any;
  product!: Product[];
  product$: Observable<Product[]> | undefined;
  companies$: Observable<Company[]> | null = null;
  licenseType = LicenseType;
  token: string = '';
  userId: number = -1;

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
    private companyService: CompanyService,
  ) {
  }

  ngOnInit(): void {
    this.companies$ = this.companyService.getCompanies();

    this.route.paramMap.subscribe((params) => {
      this.product$ = this.route.paramMap.pipe(
        tap((params: ParamMap) => console.log('product.id = ', params.get('id'))),
        switchMap((params: ParamMap) => this.productService.getProductById(Number(params.get('id')))),
        tap(console.log),
      );

      this.subscription = this.route.paramMap
        .pipe(
          tap(console.log),
          switchMap((params: ParamMap) => {
            // new item if no id
            if (!params.get('id')) {
              // create empty product
              return of([{
                name: '',
                website: '',
                licenseType: LicenseType.copyright,
              }]);
            } else {
              // get product by id
              return this.productService.getProductById(Number(params.get('id')));
            }
          }),
          tap(console.log),
        )
        .subscribe((product) => {
          this.product = product;
        });
    });

    this.authService.currentUser$.pipe(
      switchMap((user) => {
        return this.authService.currentUser$;
      }),
    ).subscribe((user) => {
      this.token = user.token;
      this.userId = user.id;
      console.log('token:', this.token);
    });
  }

  onSubmit(): any {
    console.log('onSubmit', this.product);

    console.log(this.product);

    if (this.product[0].id >= 0) {
      console.log('update product');
      this.productService
        .updateProduct(this.product[0], 'Bearer ' + this.token)
        .subscribe(() => this.router.navigate(['..'], { relativeTo: this.route }));
    } else {
      console.log('create product');
      this.productService
        .addProduct(this.product[0], 'Bearer ' + this.token)
        .subscribe((data) => this.router.navigate(['..'], { relativeTo: this.route }));
    }
  }
}
