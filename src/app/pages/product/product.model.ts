export enum LicenseType {
  permissive = 'permissive',
  copyleft = 'copyleft',
  copyright = 'copyright',
}

export class Product {
  id: number;
  userId: number;
  companyId: number;
  name: string;
  website: string;
  releaseDate: Date;
  licenseType: LicenseType;

  constructor(
    id: number,
    userId: number,
    companyId: number,
    name: string,
    website: string,
    releaseDate: Date,
    licenseType: LicenseType,
  ) {
    this.id = id;
    this.userId = userId;
    this.companyId = companyId;
    this.name = name;
    this.website = website;
    this.releaseDate = releaseDate;
    this.licenseType = licenseType;
  }
}
