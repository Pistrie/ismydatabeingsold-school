import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { LicenseType, Product } from './product.model';
import { environment } from '../../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { formatDate } from '@angular/common';
import { Company } from '../company/company.model';

const httpOptions = {
  observe: 'body',
  responseType: 'json',
};

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  // private products: Product[] = [
  //   {
  //     id: 0,
  //     companyId: 0,
  //     name: 'Facebook',
  //     website: 'facebook.com',
  //     releaseDate: new Date('2004-02-04'),
  //     licenseType: LicenseType.copyright,
  //   },
  //   {
  //     id: 1,
  //     companyId: 1,
  //     name: 'iOS',
  //     website: 'apple.com/ios',
  //     releaseDate: new Date('2007-06-29'),
  //     licenseType: LicenseType.copyright,
  //   },
  //   {
  //     id: 2,
  //     companyId: 2,
  //     name: 'Echo',
  //     website: 'alexa.amazon.com',
  //     releaseDate: new Date('2014-11-06'),
  //     licenseType: LicenseType.copyright,
  //   },
  //   {
  //     id: 3,
  //     companyId: 3,
  //     name: 'Netflix',
  //     website: 'netflix.com',
  //     releaseDate: new Date('1998-04-14'),
  //     licenseType: LicenseType.copyright,
  //   },
  //   {
  //     id: 4,
  //     companyId: 4,
  //     name: 'Google Search',
  //     website: 'google.com',
  //     releaseDate: new Date('1997-01-01'),
  //     licenseType: LicenseType.copyright,
  //   },
  // ];

  constructor(private readonly http: HttpClient) {
    // console.log('ProductService constructor called');
  }

  addProduct(product: Product, token?: any): Observable<Product> {
    console.log('addProduct called');

    const endpoint = `${environment.SERVER_API_URL}product`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .post<Product[]>(endpoint, product, httpOptions)
      .pipe(catchError(this.handleError));
  }

  getProducts(options?: any): Observable<Product[]> {
    console.log('getProducts called');

    const endpoint = `${environment.SERVER_API_URL}product`;
    console.log(endpoint);

    return this.http
      .get<Product[]>(endpoint, { ...options, ...httpOptions })
      .pipe(catchError(this.handleError));
  }

  getProductById(id: number | string, options?: any): Observable<Product[]> {
    console.log('getProductById called');

    const endpoint = `${environment.SERVER_API_URL}product/${id}`;

    return this.http
      .get<Product[]>(endpoint, { ...options, ...httpOptions })
      .pipe(catchError(this.handleError));
  }

  updateProduct(product: Product, token?: any): Observable<Product> {
    console.log('updateProduct called');

    const endpoint = `${environment.SERVER_API_URL}product/${product.id}`;

    let productJson: any = {
      companyId: product.companyId,
      name: product.name,
      website: product.website,
      releaseDate: formatDate(product.releaseDate, 'yyyy-MM-dd', 'en-US'),
      licenseType: product.licenseType,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .put<Company[]>(endpoint, productJson, httpOptions)
      .pipe(catchError(this.handleError));
  }

  deleteProduct(id: string | number, token?: any): Observable<Product[]> {
    console.log('deleteProduct called');

    const endpoint = `${environment.SERVER_API_URL}product/${id}`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .delete<Product[]>(endpoint, httpOptions)
      .pipe(catchError(this.handleError));
  }

  public handleError(error: HttpErrorResponse): Observable<any> {
    console.log(error);

    const errorResponse = {
      type: 'error',
      message: error.error.message || error.message,
    };
    return throwError(errorResponse);
  }
}
