import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Product } from '../product.model';
import { ProductService } from '../product.service';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss'],
})
export class ProductDetailComponent implements OnInit {
  product$!: Observable<Product[]>;
  token: string = '';

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private productService: ProductService,
  ) {
  }

  ngOnInit(): void {
    this.product$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.productService.getProductById(params.get('id')!)),
    );
  }

  deleteProduct(id: string | number) {
    this.authService.currentUser$.pipe(
      switchMap((user) => {
        return this.authService.currentUser$;
      }),
    ).subscribe((user) => {
      this.token = user.token;
      console.log('token:', this.token);
    });

    this.productService
      .deleteProduct(id, 'Bearer ' + this.token)
      .subscribe(() => this.router.navigate(['..'], { relativeTo: this.route }));
  }
}
