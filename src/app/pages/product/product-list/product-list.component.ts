import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Product } from '../product.model';
import { ProductService } from '../product.service';
import { User } from '../../user/user.model';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss'],
})
export class ProductListComponent implements OnInit {
  products$!: Observable<Product[]>;
  selectedId = 0;
  loggedInUser$!: Observable<User>;

  constructor(private authService: AuthService, private productService: ProductService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.products$ = this.productService.getProducts();
    this.loggedInUser$ = this.authService.currentUser$;
  }
}
