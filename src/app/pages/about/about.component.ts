import { Component, OnInit } from '@angular/core';
import { UseCase } from './usecases/usecase.model';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
})
export class AboutComponent implements OnInit {
  readonly PLAIN_USER = 'Reguliere gebruiker';
  readonly ADMIN_USER = 'Administrator';

  useCases: UseCase[] = [
    {
      id: 'UC-01',
      name: 'Inloggen',
      description: 'Hiermee logt een bestaande gebruiker in.',
      scenario: [
        'Gebruiker vult email en password in en klikt op Login knop.',
        'De applicatie valideert de ingevoerde gegevens.',
        'Indien gegevens correct zijn dan redirect de applicatie naar het startscherm.',
      ],
      actor: this.PLAIN_USER,
      precondition: 'Geen',
      postcondition: 'De actor is ingelogd',
    },
    {
      id: 'UC-02',
      name: 'Gebruikers bekijken',
      description: 'Een administrator kan een lijst van bestaande gebruikers bekijken.',
      scenario: ['Admin klikt op Users knop.', 'Applicatie toont een lijst met gebruikers.'],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Het doel is bereikt',
    },
    {
      id: 'UC-03',
      name: 'Details van gebruiker bekijken',
      description: 'Hiermee kan een administrator de details van een gebruiker bekijken.',
      scenario: [
        'Admin klikt op Users knop.',
        'Admin klikt op de gebruiker die hij wilt inspecteren.',
        'Applicatie toont de details van de gebruiker.',
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Het doel is bereikt',
    },
    {
      id: 'UC-04',
      name: 'Gebruiker toevoegen',
      description: 'Hiermee kan een administrator een nieuwe gebruiker aanmaken.',
      scenario: [
        'Admin klikt op de Users knop.',
        'Admin klikt op de New knop.',
        'Admin voert de gegevens van de nieuwe gebruiker in.',
        'Admin klikt op de Save knop.',
        'Applicatie verwijdert de gebruiker uit de database.',
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Het doel is bereikt',
    },
    {
      id: 'UC-05',
      name: 'Gebruiker bewerken',
      description: 'Hiermee kan een administrator een gebruiker bewerken.',
      scenario: [
        'Admin klikt op de Users knop.',
        'Admin klikt op de te bewerken gebruiker.',
        'Admin wijzigt de gegevens.',
        'Admin klikt op de Save knop.',
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Het doel is bereikt',
    },
    {
      id: 'UC-06',
      name: 'Gebruiker verwijderen',
      description: 'Hiermee kan een administrator een gebruiker verwijderen.',
      scenario: [
        'Admin klikt op de Users knop.',
        'Admin klikt op de te verwijderen gebruiker.',
        'Admin klikt op de Delete knop.',
      ],
      actor: this.ADMIN_USER,
      precondition: 'De actor is ingelogd',
      postcondition: 'Het doel is bereikt',
    },
    // {
    //   id: 'UC-xx',
    //   name: 'Naam',
    //   description: 'Hiermee kan een administrator ...',
    //   scenario: ['Stap 1', 'Stap 2', 'Stap 3'],
    //   actor: this.ADMIN_USER,
    //   precondition: 'De actor is ingelogd',
    //   postcondition: 'Het doel is bereikt.',
    // },
  ];

  constructor() {}

  ngOnInit() {}
}
