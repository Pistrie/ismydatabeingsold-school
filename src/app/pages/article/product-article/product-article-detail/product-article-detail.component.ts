import { Component, OnInit } from '@angular/core';
import { ProductArticle } from '../product-article.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ArticleService } from '../../article.service';
import { Observable } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ProductService } from '../../../product/product.service';
import { Product } from '../../../product/product.model';
import { ArticleType } from '../../article-type/article-type.model';
import { AuthService } from '../../../../auth/auth.service';

@Component({
  selector: 'app-product-article-detail',
  templateUrl: './product-article-detail.component.html',
  styleUrls: ['./product-article-detail.component.scss'],
})
export class ProductArticleDetailComponent implements OnInit {
  productArticle$!: Observable<ProductArticle>;
  articleTypes$!: Observable<ArticleType[]>;
  product$!: Observable<Product[]>;
  token: string = '';

  constructor(
    private route: ActivatedRoute,
    private articleService: ArticleService,
    private productService: ProductService,
    public authService: AuthService,
    private router: Router,
  ) {
  }

  ngOnInit(): void {
    this.articleTypes$ = this.articleService.getArticleTypes();

    this.product$ = this.route.paramMap.pipe(
      switchMap((params) => this.productService.getProductById(params.get('id')!)),
    );
  }

  deleteArticleType(id: string | number) {
    this.authService.currentUser$.pipe(
      switchMap((user) => {
        return this.authService.currentUser$;
      }),
    ).subscribe((user) => {
      this.token = user.token;
      console.log('token:', this.token);
    });

    this.articleService
      .deleteArticleType(id, 'Bearer ' + this.token)
      .subscribe(() => this.router.navigate(['..'], { relativeTo: this.route }));
  }
}
