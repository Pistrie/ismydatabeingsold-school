import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductArticleDetailComponent } from './product-article-detail.component';

describe('ProductArticleDetailComponent', () => {
  let component: ProductArticleDetailComponent;
  let fixture: ComponentFixture<ProductArticleDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductArticleDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductArticleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
