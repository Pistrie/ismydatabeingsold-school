import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductArticleColumnsComponent } from './product-article-columns.component';

describe('ProductArticleColumnsComponent', () => {
  let component: ProductArticleColumnsComponent;
  let fixture: ComponentFixture<ProductArticleColumnsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductArticleColumnsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductArticleColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
