import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductArticleDetailPlaceholderComponent } from './product-article-detail-placeholder.component';

describe('ProductArticleDetailPlaceholderComponent', () => {
  let component: ProductArticleDetailPlaceholderComponent;
  let fixture: ComponentFixture<ProductArticleDetailPlaceholderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductArticleDetailPlaceholderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductArticleDetailPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
