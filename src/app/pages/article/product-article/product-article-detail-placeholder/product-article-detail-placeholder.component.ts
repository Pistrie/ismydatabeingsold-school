import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-article-detail-placeholder',
  templateUrl: './product-article-detail-placeholder.component.html',
  styleUrls: ['./product-article-detail-placeholder.component.scss']
})
export class ProductArticleDetailPlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
