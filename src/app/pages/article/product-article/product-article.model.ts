import { ArticleType } from '../article-type/article-type.model';

export class ProductArticle {
  productId: number;
  articleTypes: ArticleType[];

  constructor(productId: number, articleTypes: ArticleType[]) {
    this.productId = productId;
    this.articleTypes = articleTypes;
  }
}
