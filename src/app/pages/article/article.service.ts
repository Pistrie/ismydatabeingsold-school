import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ArticleType, ArticleTypeEnum } from './article-type/article-type.model';
import { ProductArticle } from './product-article/product-article.model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Company } from '../company/company.model';


const httpOptions = {
  observe: 'body',
  responseType: 'json',
};

@Injectable({
  providedIn: 'root',
})
export class ArticleService {
  // private articleTypes: ArticleType[] = [
  //   {
  //     id: 0,
  //     productId: 0,
  //     dateOfWriting: new Date(Date.now()),
  //     articleType: ArticleTypeEnum.priv,
  //     description:
  //       'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem ipsam illo cupiditate porro, officia, eligendi quae ab laboriosam accusantium accusamus repellat cumque. Ratione perspiciatis adipisci id, ad est mollitia facilis!',
  //   },
  //   {
  //     id: 1,
  //     productId: 0,
  //     dateOfWriting: new Date(Date.now()),
  //     articleType: ArticleTypeEnum.privusage,
  //     description:
  //       'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem ipsam illo cupiditate porro, officia, eligendi quae ab laboriosam accusantium accusamus repellat cumque. Ratione perspiciatis adipisci id, ad est mollitia facilis!',
  //   },
  //   {
  //     id: 2,
  //     productId: 1,
  //     dateOfWriting: new Date(Date.now()),
  //     articleType: ArticleTypeEnum.priv,
  //     description:
  //       'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem ipsam illo cupiditate porro, officia, eligendi quae ab laboriosam accusantium accusamus repellat cumque. Ratione perspiciatis adipisci id, ad est mollitia facilis!',
  //   },
  //   {
  //     id: 3,
  //     productId: 1,
  //     dateOfWriting: new Date(Date.now()),
  //     articleType: ArticleTypeEnum.privusage,
  //     description:
  //       'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem ipsam illo cupiditate porro, officia, eligendi quae ab laboriosam accusantium accusamus repellat cumque. Ratione perspiciatis adipisci id, ad est mollitia facilis!',
  //   },
  //   {
  //     id: 4,
  //     productId: 2,
  //     dateOfWriting: new Date(Date.now()),
  //     articleType: ArticleTypeEnum.priv,
  //     description:
  //       'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem ipsam illo cupiditate porro, officia, eligendi quae ab laboriosam accusantium accusamus repellat cumque. Ratione perspiciatis adipisci id, ad est mollitia facilis!',
  //   },
  //   {
  //     id: 5,
  //     productId: 2,
  //     dateOfWriting: new Date(Date.now()),
  //     articleType: ArticleTypeEnum.privusage,
  //     description:
  //       'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem ipsam illo cupiditate porro, officia, eligendi quae ab laboriosam accusantium accusamus repellat cumque. Ratione perspiciatis adipisci id, ad est mollitia facilis!',
  //   },
  //   {
  //     id: 6,
  //     productId: 3,
  //     dateOfWriting: new Date(Date.now()),
  //     articleType: ArticleTypeEnum.priv,
  //     description:
  //       'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem ipsam illo cupiditate porro, officia, eligendi quae ab laboriosam accusantium accusamus repellat cumque. Ratione perspiciatis adipisci id, ad est mollitia facilis!',
  //   },
  //   {
  //     id: 7,
  //     productId: 3,
  //     dateOfWriting: new Date(Date.now()),
  //     articleType: ArticleTypeEnum.privusage,
  //     description:
  //       'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem ipsam illo cupiditate porro, officia, eligendi quae ab laboriosam accusantium accusamus repellat cumque. Ratione perspiciatis adipisci id, ad est mollitia facilis!',
  //   },
  //   {
  //     id: 8,
  //     productId: 4,
  //     dateOfWriting: new Date(Date.now()),
  //     articleType: ArticleTypeEnum.priv,
  //     description:
  //       'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem ipsam illo cupiditate porro, officia, eligendi quae ab laboriosam accusantium accusamus repellat cumque. Ratione perspiciatis adipisci id, ad est mollitia facilis!',
  //   },
  //   {
  //     id: 9,
  //     productId: 4,
  //     dateOfWriting: new Date(Date.now()),
  //     articleType: ArticleTypeEnum.privusage,
  //     description:
  //       'Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatem ipsam illo cupiditate porro, officia, eligendi quae ab laboriosam accusantium accusamus repellat cumque. Ratione perspiciatis adipisci id, ad est mollitia facilis!',
  //   },
  // ];

  constructor(private readonly http: HttpClient) {
    // console.log('ArticleService constructor called');
  }

  addArticleType(articleType: ArticleType, token?: any): Observable<ArticleType> {
    console.log('addArticleType called');

    const endpoint = `${environment.SERVER_API_URL}product-article`;

    let articleTypeJson = {
      productId: articleType.productId,
      articleType: articleType.articleType,
      description: articleType.description,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .post<ArticleType>(endpoint, articleTypeJson, httpOptions)
      .pipe(catchError(this.handleError));
  }

  getArticleTypes(options?: any): Observable<ArticleType[]> {
    console.log('getArticleTypes called');

    const endpoint = `${environment.SERVER_API_URL}product-article`;
    console.log(endpoint);

    return this.http
      .get<ArticleType[]>(endpoint, { ...options, ...httpOptions })
      .pipe(catchError(this.handleError));
  }

  getArticleTypeById(id: number | string, options?: any): Observable<ArticleType[]> {
    console.log('getArticleById called');

    const endpoint = `${environment.SERVER_API_URL}product-article/${id}`;
    console.log(endpoint);

    return this.http
      .get<ArticleType[]>(endpoint, { ...options, ...httpOptions })
      .pipe(catchError(this.handleError));
  }

  updateArticleType(articleType: ArticleType, token?: any): Observable<ArticleType> {
    console.log('updateArticleType called');

    const endpoint = `${environment.SERVER_API_URL}product-article/${articleType.id}`;

    let articleTypeJson: any = {
      productId: articleType.productId,
      articleType: articleType.articleType,
      description: articleType.description,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .put<Company[]>(endpoint, articleTypeJson, httpOptions)
      .pipe(catchError(this.handleError));
  }

  deleteArticleType(id: string | number, token?: any): Observable<ArticleType[]> {
    console.log('deleteArticleType called');

    const endpoint = `${environment.SERVER_API_URL}product-article/${id}`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .delete<ArticleType[]>(endpoint, httpOptions)
      .pipe(catchError(this.handleError));
  }

  public handleError(error: HttpErrorResponse): Observable<any> {
    console.log(error);

    const errorResponse = {
      type: 'error',
      message: error.error.message || error.message,
    };
    return throwError(errorResponse);
  }
}
