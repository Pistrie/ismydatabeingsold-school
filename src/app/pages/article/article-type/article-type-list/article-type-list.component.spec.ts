import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleTypeListComponent } from './article-type-list.component';

describe('ArticleTypeListComponent', () => {
  let component: ArticleTypeListComponent;
  let fixture: ComponentFixture<ArticleTypeListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleTypeListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
