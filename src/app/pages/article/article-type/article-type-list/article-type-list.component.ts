import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ArticleService } from '../../article.service';
import { ArticleType } from '../article-type.model';
import { Product } from '../../../product/product.model';
import { map } from 'rxjs/operators';
import { User } from '../../../user/user.model';
import { AuthService } from '../../../../auth/auth.service';

@Component({
  selector: 'app-article-type-list',
  templateUrl: './article-type-list.component.html',
  styleUrls: ['./article-type-list.component.scss'],
})
export class ArticleTypeListComponent implements OnInit {
  subscription: any;
  articleTypes$!: Observable<ArticleType[]>;
  products$!: Observable<Product[]>;
  loggedInUser$!: Observable<User>;

  constructor(private articleService: ArticleService, private authService: AuthService) {
  }

  ngOnInit(): void {
    this.articleTypes$ = this.articleService.getArticleTypes();
    this.loggedInUser$ = this.authService.currentUser$;
  }

  // findProduct(products: Product[], id: number | string): Product {
  //   return products.find((product) => product.id === +id)!;
  // }

  findProductFromObservableProducts(products$: Observable<Product[]>, id: number | string): Product {
    let productsArray: any;
    this.products$.pipe(
      map((products: Product[]) => productsArray = products),
    );

    return productsArray.find((product: Product) => product.id === +id)!;
  }
}
