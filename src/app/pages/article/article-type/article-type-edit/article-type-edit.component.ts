import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ArticleType, ArticleTypeEnum } from '../article-type.model';
import { Observable, of } from 'rxjs';
import { Product } from '../../../product/product.model';
import { ArticleService } from '../../article.service';
import { ProductService } from '../../../product/product.service';
import { switchMap, tap } from 'rxjs/operators';
import { AuthService } from '../../../../auth/auth.service';

@Component({
  selector: 'app-article-type-edit',
  templateUrl: './article-type-edit.component.html',
  styleUrls: ['./article-type-edit.component.scss'],
})
export class ArticleTypeEditComponent implements OnInit {
  subscription: any;
  articleType!: ArticleType[];
  articleType$: Observable<ArticleType> | undefined;
  products$: Observable<Product[]> | null = null;
  articleTypeEnum = ArticleTypeEnum;
  token: string = '';
  userId: number = -1;

  constructor(private authService: AuthService, private route: ActivatedRoute, private router: Router, private articleService: ArticleService, private productService: ProductService) {
  }

  ngOnInit(): void {
    this.products$ = this.productService.getProducts();

    this.route.paramMap.subscribe(() => {
      this.articleType$ = this.route.paramMap.pipe(
        tap((params) => console.log('articleType.id = ', params.get('id'))),
        switchMap((params) => this.articleService.getArticleTypeById(Number(params.get('id')))),
        tap(console.log),
      );

      this.subscription = this.route.paramMap
        .pipe(
          tap(console.log),
          switchMap((params: ParamMap) => {
            // new item if no id
            if (!params.get('id')) {
              // create empty articleType
              return of([{
                description: '',
              }]);
            } else {
              // get articleType by id
              return this.articleService.getArticleTypeById(Number(params.get('id')));
            }
          }),
          tap(console.log),
        )
        .subscribe((articleType) => {
          this.articleType = articleType;
        });
    });

    this.authService.currentUser$.pipe(
      switchMap((user) => {
        return this.authService.currentUser$;
      }),
    ).subscribe((user) => {
      this.token = user.token;
      this.userId = user.id;
      console.log('token:', this.token);
    });
  }

  onSubmit(): any {
    console.log('onSubmit', this.articleType);
    this.articleType[0].dateOfWriting = new Date(Date.now());

    console.log(this.articleType);

    if (this.articleType[0].id >= 0) {
      console.log('update articleType');
      this.articleService
        .updateArticleType(this.articleType[0], 'Bearer ' + this.token)
        .subscribe(() => this.router.navigate(['../..'], { relativeTo: this.route }));
    } else {
      console.log('create articleType');
      this.articleService
        .addArticleType(this.articleType[0], 'Bearer ' + this.token)
        .subscribe(() => this.router.navigate(['..'], { relativeTo: this.route }));
    }
  }
}
