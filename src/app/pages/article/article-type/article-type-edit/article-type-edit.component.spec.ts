import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleTypeEditComponent } from './article-type-edit.component';

describe('ArticleTypeEditComponent', () => {
  let component: ArticleTypeEditComponent;
  let fixture: ComponentFixture<ArticleTypeEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleTypeEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleTypeEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
