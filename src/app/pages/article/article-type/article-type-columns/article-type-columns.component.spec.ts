import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleTypeColumnsComponent } from './article-type-columns.component';

describe('ArticleTypeColumnsComponent', () => {
  let component: ArticleTypeColumnsComponent;
  let fixture: ComponentFixture<ArticleTypeColumnsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleTypeColumnsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleTypeColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
