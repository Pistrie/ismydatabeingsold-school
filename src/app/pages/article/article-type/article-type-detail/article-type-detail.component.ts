import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Company } from 'src/app/pages/company/company.model';
import { CompanyService } from 'src/app/pages/company/company.service';
import { ArticleService } from '../../article.service';
import { ArticleType } from '../article-type.model';
import { AuthService } from '../../../../auth/auth.service';

@Component({
  selector: 'app-article-type-detail',
  templateUrl: './article-type-detail.component.html',
  styleUrls: ['./article-type-detail.component.scss'],
})
export class ArticleTypeDetailComponent implements OnInit {
  articleType$!: Observable<ArticleType[]>;
  company$!: Observable<Company[]>;
  token: string = '';

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private articleService: ArticleService,
    private companyService: CompanyService,
  ) {
  }

  ngOnInit(): void {
    this.articleType$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.articleService.getArticleTypeById(params.get('id')!)),
    );
    this.company$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.companyService.getCompanyById(params.get('id')!)),
    );
  }

  deleteArticleType(id: string | number) {
    this.authService.currentUser$.pipe(
      switchMap((user) => {
        return this.authService.currentUser$;
      }),
    ).subscribe((user) => {
      this.token = user.token;
      console.log('token:', this.token);
    });

    this.articleService
      .deleteArticleType(id, 'Bearer ' + this.token)
      .subscribe(() => this.router.navigate(['..'], { relativeTo: this.route }));
  }
}
