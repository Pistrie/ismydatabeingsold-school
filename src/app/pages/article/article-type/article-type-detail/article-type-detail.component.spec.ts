import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleTypeDetailComponent } from './article-type-detail.component';

describe('ArticleTypeDetailComponent', () => {
  let component: ArticleTypeDetailComponent;
  let fixture: ComponentFixture<ArticleTypeDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleTypeDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleTypeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
