export enum ArticleTypeEnum {
  priv = 'What is collected?',
  privusage = 'How is the data used?',
}

export class ArticleType {
  id: number;
  userId: number;
  productId: number;
  dateOfWriting: Date;
  articleType: string;
  description: string;

  constructor(
    id: number,
    userId: number,
    productId: number,
    dateOfWriting: Date,
    articleType: string,
    description: string,
  ) {
    this.id = id;
    this.userId = userId;
    this.productId = productId;
    this.dateOfWriting = dateOfWriting;
    this.articleType = articleType;
    this.description = description;
  }
}
