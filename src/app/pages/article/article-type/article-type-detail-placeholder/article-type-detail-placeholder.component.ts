import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-article-type-detail-placeholder',
  templateUrl: './article-type-detail-placeholder.component.html',
  styleUrls: ['./article-type-detail-placeholder.component.scss']
})
export class ArticleTypeDetailPlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
