import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticleTypeDetailPlaceholderComponent } from './article-type-detail-placeholder.component';

describe('ArticleTypeDetailPlaceholderComponent', () => {
  let component: ArticleTypeDetailPlaceholderComponent;
  let fixture: ComponentFixture<ArticleTypeDetailPlaceholderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticleTypeDetailPlaceholderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleTypeDetailPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
