import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { UserDetailComponent } from './user-detail.component';
import { User, UserRole } from '../user.model';
import { BehaviorSubject, of } from 'rxjs';
import { AuthService } from '../../../auth/auth.service';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { UserService } from '../user.service';

// global mock objects
const expectedUserData: User[] = [
  {
    id: 1,
    firstName: 'User',
    lastName: 'One',
    emailAddress: 'userone@host.com',
    dateOfBirth: new Date('2000-11-23'),
    role: UserRole.admin,
    password: 'password123',
    token: '',
  },
];

describe('UserDetailComponent', () => {
  let component: UserDetailComponent;
  let fixture: ComponentFixture<UserDetailComponent>;

  let authServiceSpy;
  let activatedRouteSpy;
  let routerSpy;
  let userServiceSpy;

  beforeEach(() => {
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'register',
      'logout',
      'getUserFromLocalStorage',
      'saveUserToLocalStorage',
      'userMayEdit',
    ]);
    const mockUser$ = new BehaviorSubject<User>(expectedUserData[0]);
    authServiceSpy.currentUser$ = mockUser$;

    activatedRouteSpy = jasmine.createSpyObj('ActivatedRoute', ['paramMap']);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    userServiceSpy = jasmine.createSpyObj('UserService', [
      'addUser',
      'getUsers',
      'getUserById',
      'updateUser',
      'deleteUser',
    ]);

    TestBed.configureTestingModule({
      declarations: [
        UserDetailComponent,
      ],
      imports: [],
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: UserService, useValue: userServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: 1,
              }),
            ),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(UserDetailComponent);
    component = fixture.componentInstance;
  });

  fit('should create', (done) => {
    userServiceSpy.getUserById.and.returnValue(of(expectedUserData));

    fixture.detectChanges();
    expect(component).toBeTruthy();
    component.user$.subscribe((result) => {
      expect(result).toEqual(expectedUserData);
    });
    done();
  });
});
