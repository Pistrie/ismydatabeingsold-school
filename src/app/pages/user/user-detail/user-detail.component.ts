import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { User } from '../user.model';
import { UserService } from '../user.service';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss'],
})
export class UserDetailComponent implements OnInit {
  user$!: Observable<User[]>;
  currentUser!: User;
  token: string = '';

  constructor(public authService: AuthService, private route: ActivatedRoute, private router: Router, private userService: UserService) {
  }

  ngOnInit(): void {
    this.user$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.userService.getUserById(params.get('id')!)),
    );

    this.authService.currentUser$.pipe(
      switchMap((user) => {
        return this.authService.currentUser$;
      }),
    ).subscribe((user) => {
      this.currentUser = user;
      // fixes error in log because user does not exist when logging out
      if (this.currentUser) {
        this.token = this.currentUser.token;
      }
    });
  }

  deleteUser(id: string | number, token?: any) {
    this.userService
      .deleteUser(id, 'Bearer ' + this.token)
      .subscribe(() => this.router.navigate(['..'], { relativeTo: this.route }));

    this.authService.logout();
  }
}
