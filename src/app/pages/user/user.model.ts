export enum UserRole {
  admin = 'admin',
  user = 'user',
}

export class User {
  id: number = 0;
  firstName: string;
  lastName: string;
  dateOfBirth: Date;
  emailAddress: string;
  role: UserRole;
  password: string;
  token: string = '';

  constructor(
    firstName: string,
    lastName: string,
    emailAdress: string,
    dateOfBirth: Date,
    role: UserRole,
    password: string,
    token?: string,
    id?: number,
  ) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.emailAddress = emailAdress;
    this.dateOfBirth = dateOfBirth;
    this.role = role;
    this.password = password;
    if (token) {
      this.token = token;
    }
    if (id) {
      this.id = id;
    }
  }
}
