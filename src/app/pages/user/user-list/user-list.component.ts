import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from '../user.model';
import { UserService } from '../user.service';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
  users$!: Observable<User[]>;
  selectedId = 0;
  loggedInUser$!: Observable<User>;

  constructor(private authService: AuthService, private userService: UserService) {
  }

  ngOnInit(): void {
    this.users$ = this.userService.getUsers();
    this.loggedInUser$ = this.authService.currentUser$;
  }
}
