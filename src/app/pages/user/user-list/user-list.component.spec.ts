import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject, of } from 'rxjs';
import { User, UserRole } from '../user.model';
import { UserService } from '../user.service';
import { UserListComponent } from './user-list.component';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../../../auth/auth.service';
import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[routerLink]',
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick(): void {
    this.navigatedTo = this.linkParams;
  }
}

// global mock objects
const expectedUserData: User[] = [
  {
    id: 1,
    firstName: 'User',
    lastName: 'One',
    emailAddress: 'userone@host.com',
    dateOfBirth: new Date('2000-11-23'),
    role: UserRole.admin,
    password: 'password123',
    token: '',
  },
];

describe('UserListComponent', () => {
  // the real component-under-test - we dont mock this
  let component: UserListComponent;
  let fixture: ComponentFixture<UserListComponent>;

  // mock services needed by the constructor
  let authServiceSpy;
  let userServiceSpy;

  beforeEach(() => {
    // initialise the services as jasmine spy objects
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'register',
      'logout',
      'getUserFromLocalStorage',
      'saveUserToLocalStorage',
      'userMayEdit',
    ]);
    const mockUser$ = new BehaviorSubject<User>(expectedUserData[0]);
    authServiceSpy.currentUser$ = mockUser$;

    userServiceSpy = jasmine.createSpyObj('UserService', [
      'addUser',
      'getUsers',
      'getUsersById',
      'updateUser',
      'deleteUser',
    ]);

    TestBed.configureTestingModule({
      // the declared components needed to test the UserListComponent
      declarations: [
        UserListComponent, // the real component that we will test
        RouterLinkStubDirective, // Stubbed (Mocked) component required to instantiate the real component
      ],
      //
      // The constructor of our real component uses dependency injected services
      // Never provide tje real services in testcases!
      //
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: UserService, useValue: userServiceSpy },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(UserListComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  fit('should create', (done) => {
    userServiceSpy.getUsers.and.returnValue(of(expectedUserData));

    fixture.detectChanges();
    expect(component).toBeTruthy();
    component.users$.subscribe((result) => {
      expect(result).toEqual(expectedUserData);
    });
    done();
  });
});
