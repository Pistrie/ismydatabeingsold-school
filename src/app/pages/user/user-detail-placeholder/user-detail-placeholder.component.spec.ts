import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserDetailPlaceholderComponent } from './user-detail-placeholder.component';

describe('UserDetailPlaceholderComponent', () => {
  let component: UserDetailPlaceholderComponent;
  let fixture: ComponentFixture<UserDetailPlaceholderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserDetailPlaceholderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserDetailPlaceholderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
