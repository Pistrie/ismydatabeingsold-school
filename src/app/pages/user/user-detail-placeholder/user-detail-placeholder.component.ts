import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-detail-placeholder',
  templateUrl: './user-detail-placeholder.component.html',
  styleUrls: ['./user-detail-placeholder.component.scss']
})
export class UserDetailPlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
