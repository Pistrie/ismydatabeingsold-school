import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { UserEditComponent } from './user-edit.component';
import { BehaviorSubject, of, Subscription } from 'rxjs';
import { User, UserRole } from '../user.model';
import { AuthService } from '../../../auth/auth.service';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { UserService } from '../user.service';
import { EnumToArrayPipe } from '../../../enum-to-array.pipe';
import { Directive, HostListener, Input, Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'enumToArray' })
class MockPipe implements PipeTransform {
  transform(data: Object) {
    return Object.values(data);
  }
}

@Directive({
  selector: '[routerLink]',
})
export class RouterLinkStubDirective {
  @Input('routerLink') linkParams: any;
  navigatedTo: any = null;

  @HostListener('click')
  onClick(): void {
    this.navigatedTo = this.linkParams;
  }
}

// global mock objects
const currentUser: User[] = [
  {
    id: 1,
    firstName: 'User',
    lastName: 'One',
    emailAddress: 'userone@host.com',
    dateOfBirth: new Date('2000-11-23'),
    role: UserRole.admin,
    password: 'password123',
    token: '',
  },
];

describe('UserEditComponent', () => {
  // the real component-under-test - we dont mock this
  let component: UserEditComponent;
  let fixture: ComponentFixture<UserEditComponent>;

  // mock services needed by the constructor
  let authServiceSpy;
  let activatedRouteSpy;
  let routerSpy;
  let userServiceSpy;


  beforeEach(() => {
    // initialise the services as jasmine spy objects
    authServiceSpy = jasmine.createSpyObj('AuthService', [
      'login',
      'register',
      'logout',
      'getUserFromLocalStorage',
      'saveUserToLocalStorage',
      'userMayEdit',
    ]);
    const mockUser$ = new BehaviorSubject<User>(currentUser[0]);
    authServiceSpy.currentUser$ = mockUser$;

    activatedRouteSpy = jasmine.createSpyObj('ActivatedRoute', ['paramMap']);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    userServiceSpy = jasmine.createSpyObj('UserService', [
      'addUser',
      'getUsers',
      'getUserById',
      'updateUser',
      'deleteUser',
    ]);

    TestBed.configureTestingModule({
      // The declared components needed to test the component
      declarations: [
        UserEditComponent, // The real component that we will test
        RouterLinkStubDirective, // Stubbed (Mocked) component required to instantiate the real component
        MockPipe, // Stubbed custom pipe
      ],
      imports: [FormsModule],
      //
      // The constructor of our real component uses dependency injected services
      // Never provide the real services in testcases!
      //
      providers: [
        { provide: AuthService, useValue: authServiceSpy },
        { provide: Router, useValue: routerSpy },
        { provide: UserService, useValue: userServiceSpy },
        {
          provide: ActivatedRoute,
          useValue: {
            paramMap: of(
              convertToParamMap({
                id: 1,
              }),
            ),
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(UserEditComponent);
    component = fixture.componentInstance;
  });

  afterEach(() => {
    fixture.destroy();
  });

  fit('should create', (done) => {
    userServiceSpy.getUserById.and.returnValue(of(currentUser));

    // These are required to that we can unsubscribe in ngOnDestroy
    component.subscription = new Subscription();

    fixture.detectChanges();
    expect(component).toBeTruthy();
    expect(component.user).toEqual(currentUser);
    done();
  });
});
