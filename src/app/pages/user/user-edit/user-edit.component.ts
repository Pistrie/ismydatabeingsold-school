import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { EnumToArrayPipe } from 'src/app/enum-to-array.pipe';
import { User, UserRole } from '../user.model';
import { UserService } from '../user.service';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.scss'],
})
export class UserEditComponent implements OnInit {
  subscription: any;
  user!: User[];
  user$: Observable<User> | null = null;
  userRole = UserRole;
  token: string = '';
  currentUserRole = '';

  constructor(public authService: AuthService, private route: ActivatedRoute, private router: Router, private userService: UserService) {
  }

  ngOnInit(): void {
    /**
     * We gebruiken de EditComponent om een bestaande record te wijzigen
     * én om een nieuwe record te maken.
     * Een bestaande record heeft een :id in de URL, bv '/users/1/edit'
     * Als die er dus is gaan we de user ophalen en bewerken.
     * Als er geen :id in de URL zit (via '/users/new') maken we een nieuwe record.
     */
    this.route.paramMap.subscribe((params) => {
      this.user$ = this.route.paramMap.pipe(
        tap((params: ParamMap) => console.log('user.id = ', params.get('id'))),
        switchMap((params: ParamMap) => this.userService.getUserById(Number(params.get('id')))),
        tap(console.log),
      );
    });

    this.subscription = this.route.paramMap
      .pipe(
        tap(console.log),
        switchMap((params: ParamMap) => {
          // new item if no id
          if (!params.get('id')) {
            // create empty user
            return of([{
              firstName: '',
              lastName: '',
              emailAddress: '',
              role: UserRole.user,
              password: '',
            }]);
          } else {
            // get user by id
            return this.userService.getUserById(Number(params.get('id')));
          }
        }),
        tap(console.log),
      )
      .subscribe((user) => {
        this.user = user;
      });

    this.authService.currentUser$.pipe(
      switchMap((user) => {
        return this.authService.currentUser$;
      }),
    ).subscribe((user) => {
      if (user) {
        this.token = user.token;
        this.currentUserRole = user.role;
      }
    });
  }

  onSubmit(): any {
    console.log('onSubmit', this.user);

    console.log(this.user);

    if (this.user[0].id >= 0) {
      // A user with id must have been saved before
      console.log('update user');
      this.userService
        .updateUser(this.user[0], 'Bearer ' + this.token)
        .subscribe(() => this.router.navigate(['..'], { relativeTo: this.route }));
    } else {
      // A user without id has not been saved to the database before
      console.log('create user');
      this.userService
        .addUser(this.user[0], 'Bearer ' + this.token)
        .subscribe((data) => this.router.navigate(['..'], { relativeTo: this.route }));
    }
  }
}
