import { formatDate } from '@angular/common';
import { Injectable } from '@angular/core';
import { from, Observable, of, throwError } from 'rxjs';
import { catchError, filter, map, take } from 'rxjs/operators';
import { User, UserRole } from './user.model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

const httpOptions = {
  observe: 'body',
  responseType: 'json',
};

@Injectable({
  providedIn: 'root',
})
export class UserService {
  // private users: User[] = [
  //   {
  //     id: 0,
  //     firstName: 'User',
  //     lastName: 'One',
  //     emailAddress: 'userone@host.com',
  //     dateOfBirth: new Date('2000-11-23'),
  //     role: UserRole.admin,
  //   },
  //   {
  //     id: 1,
  //     firstName: 'User',
  //     lastName: 'Two',
  //     emailAddress: 'usertwo@host.com',
  //     dateOfBirth: new Date('1970-03-05'),
  //     role: UserRole.user,
  //   },
  //   {
  //     id: 2,
  //     firstName: 'User',
  //     lastName: 'Three',
  //     emailAddress: 'userthree@host.com',
  //     dateOfBirth: new Date('1985-07-09'),
  //     role: UserRole.user,
  //   },
  //   {
  //     id: 3,
  //     firstName: 'User',
  //     lastName: 'Four',
  //     emailAddress: 'userfour@host.com',
  //     dateOfBirth: new Date('200-06-25'),
  //     role: UserRole.user,
  //   },
  //   {
  //     id: 4,
  //     firstName: 'User',
  //     lastName: 'Five',
  //     emailAddress: 'userfive@host.com',
  //     dateOfBirth: new Date('2001-12-15'),
  //     role: UserRole.user,
  //   },
  // ];

  constructor(private readonly http: HttpClient) {
    // console.log('Service constructor called');
  }

  addUser(user: User, token?: any): Observable<User[]> {
    console.log('addUser called');

    const endpoint = `${environment.SERVER_API_URL}user`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .post<User[]>(endpoint, user, httpOptions)
      .pipe(catchError(this.handleError));
  }

  getUsers(options?: any): Observable<User[]> {
    // 'of' maakt een Observable<User[]>
    console.log('getUsers called');

    const endpoint = `${environment.SERVER_API_URL}user`;
    console.log(endpoint);

    return this.http
      .get<User[]>(endpoint, { ...options, ...httpOptions })
      .pipe(catchError(this.handleError));
  }

  getUserById(id: number | string, options?: any): Observable<User[]> {
    console.log('getUserById called');

    const endpoint = `${environment.SERVER_API_URL}user/${id}`;

    return this.http
      .get<User[]>(endpoint, { ...options, ...httpOptions })
      .pipe(catchError(this.handleError));
  }

  updateUser(user: User, token?: any): Observable<User[]> {
    console.log('updateUser called');

    const endpoint = `${environment.SERVER_API_URL}user/${user.id}`;

    let userJson: any = {
      firstName: user.firstName,
      lastName: user.lastName,
      emailAddress: user.emailAddress,
      dateOfBirth: formatDate(user.dateOfBirth, 'yyyy-MM-dd', 'en-US'),
      role: user.role,
      password: user.password,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .put<User[]>(endpoint, userJson, httpOptions)
      .pipe(catchError(this.handleError));
  }

  deleteUser(id: number | string, token?: any): Observable<User[]> {
    console.log('deleteUser called');

    const endpoint = `${environment.SERVER_API_URL}user/${id}`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .delete<User[]>(endpoint, httpOptions)
      .pipe(catchError(this.handleError));
  }

  public handleError(error: HttpErrorResponse): Observable<any> {
    console.log(error);

    const errorResponse = {
      type: 'error',
      message: error.error.message || error.message,
    };
    return throwError(errorResponse);
  }
}
