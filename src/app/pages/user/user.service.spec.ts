import { TestBed } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { UserService } from './user.service';
import { of } from 'rxjs';
import { User, UserRole } from './user.model';

// Global mock objects
const expectedUserData = [
  {
    id: 1,
    firstName: 'User',
    lastName: 'One',
    emailAddress: 'userone@host.com',
    dateOfBirth: new Date('2000-11-23'),
    role: 'admin',
    password: 'password123',
    token: '',
  },
  {
    id: 2,
    firstName: 'User',
    lastName: 'Two',
    emailAddress: 'usertwo@host.com',
    dateOfBirth: new Date('2001-11-23'),
    role: 'user',
    password: 'password123',
    token: '',
  },
];

describe('UserService', () => {
  let service: UserService;
  let httpSpy: jasmine.SpyObj<HttpClient>;

  beforeEach(() => {
    httpSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);

    TestBed.configureTestingModule({
      providers: [{ provide: HttpClient, useValue: httpSpy }],
    });
    service = TestBed.inject(UserService);
    httpSpy = TestBed.inject(HttpClient) as jasmine.SpyObj<HttpClient>;
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  fit('should return a list of users', (done: DoneFn) => {
    httpSpy.get.and.returnValue(of(expectedUserData));

    service.getUsers().subscribe((users: User[]) => {
      console.log(users);
      expect(users.length).toBe(2);
      expect(users[0].id).toEqual(expectedUserData[0].id);
      done();
    });
  });

  fit('should return a user by id', (done) => {
    let user = expectedUserData.find(x => x.id == 1);
    let userArray = [user];

    httpSpy.get.and.returnValue(of(userArray));

    service.getUserById(1).subscribe((user: User[]) => {
      console.log(user[0]);
      expect(user.length).toBe(1);
      expect(user[0].id).toEqual(expectedUserData[0].id);
      done();
    });
  });

  fit('should add a user', (done) => {
    let user = {
      id: 3,
      firstName: 'User',
      lastName: 'Three',
      emailAddress: 'userthree@host.com',
      dateOfBirth: new Date('2002-11-23'),
      role: UserRole.user,
      password: 'password123',
      token: '374fh378h4f89hfsdudhvcs8oe7',
    };
    let userArray = [user];

    httpSpy.post.and.returnValue(of(userArray));

    service.addUser(userArray[0], userArray[0].token).subscribe((user: User[]) => {
      console.log(user);
      expect(user.length).toBe(1);
      expect(user[0].id).toEqual(userArray[0].id);
      done();
    });
  });

  fit('should update a user', (done) => {
    let user = {
      id: 3,
      firstName: 'User',
      lastName: 'Three',
      emailAddress: 'userthree@host.com',
      dateOfBirth: new Date('2002-11-23'),
      role: UserRole.user,
      password: 'password123',
      token: '374fh378h4f89hfsdudhvcs8oe7',
    };
    let userArray = [user];

    httpSpy.put.and.returnValue(of(userArray));

    service.updateUser(userArray[0], userArray[0].token).subscribe((user: User[]) => {
      console.log(user);
      expect(user.length).toBe(1);
      expect(user[0].id).toBe(userArray[0].id);
      done();
    });
  });

  fit('should delete a user', (done) => {
    let user = {
      id: 3,
      firstname: 'user',
      lastname: 'three',
      emailAddress: 'userthree@host.com',
      dateOfBirth: new Date('2002-11-23'),
      role: UserRole.user,
      password: 'password123',
      token: '374fh378h4f89hfsdudhvcs8oe7',
    };
    let userArray = [user];

    httpSpy.delete.and.returnValue(of(userArray));

    service.deleteUser(userArray[0].id, userArray[0].token).subscribe((user: User[]) => {
      console.log(user);
      expect(user.length).toBe(1);
      expect(user[0].id).toEqual(userArray[0].id);
      done();
    });
  });
});
