import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-company-detail-placeholder',
  templateUrl: './company-detail-placeholder.component.html',
  styleUrls: ['./company-detail-placeholder.component.scss']
})
export class CompanyDetailPlaceholderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
