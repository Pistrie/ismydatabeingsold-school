import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Company } from '../company.model';
import { CompanyService } from '../company.service';
import { AuthService } from '../../../auth/auth.service';
import { User } from '../../user/user.model';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.scss'],
})
export class CompanyListComponent implements OnInit {
  companies$!: Observable<Company[]>;
  selectedId = 0;
  loggedInUser$!: Observable<User>;

  constructor(private authService: AuthService, private companyService: CompanyService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.companies$ = this.companyService.getCompanies();
    this.loggedInUser$ = this.authService.currentUser$;
  }
}
