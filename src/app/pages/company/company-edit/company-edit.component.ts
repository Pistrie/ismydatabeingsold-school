import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';
import { UserService } from '../../user/user.service';
import { Company } from '../company.model';
import { CompanyService } from '../company.service';
import { AuthService } from '../../../auth/auth.service';
import { ensureTemplateParser } from '@angular-eslint/eslint-plugin-template/dist/utils/create-eslint-rule';

@Component({
  selector: 'app-company-edit',
  templateUrl: './company-edit.component.html',
  styleUrls: ['./company-edit.component.scss'],
})
export class CompanyEditComponent implements OnInit {
  subscription: any;
  company!: Company[];
  company$: Observable<Company[]> | undefined;
  token: string = '';

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private companyService: CompanyService,
  ) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params) => {
      this.company$ = this.route.paramMap.pipe(
        tap((params: ParamMap) => console.log('company.id = ', params.get('id'))),
        switchMap((params: ParamMap) => this.companyService.getCompanyById(Number(params.get('id')))),
        tap(console.log),
      );

      this.subscription = this.route.paramMap
        .pipe(
          switchMap((params: ParamMap) => {
            // new item if no id
            if (!params.get('id')) {
              // create empty company
              return of([{
                name: '',
                website: '',
                numberOfEmployees: 0,
              }]);
            } else {
              // get company by id
              return this.companyService.getCompanyById(Number(params.get('id')));
            }
          }),
          tap(console.log),
        )
        .subscribe((company) => {
          this.company = company;
        });
    });
  }

  onSubmit(): any {
    console.log('onSubmit', this.company);

    this.authService.currentUser$.pipe(
      switchMap((user) => {
        return this.authService.currentUser$;
      }),
    ).subscribe((user) => {
      this.token = user.token;
      console.log('token:', this.token);
    });

    console.log(this.company);

    if (this.company[0].id >= 0) {
      // A company with id must have been saved before
      console.log('update company');
      this.companyService
        .updateCompany(this.company[0], 'Bearer ' + this.token)
        .subscribe(() => this.router.navigate(['..'], { relativeTo: this.route }));
    } else {
      // A company without id has not been saved to the database before
      console.log('create company');
      this.companyService
        .addCompany(this.company[0], 'Bearer ' + this.token)
        .subscribe((data) => this.router.navigate(['..'], { relativeTo: this.route }));
    }
  }
}
