import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Company } from '../company.model';
import { CompanyService } from '../company.service';
import { AuthService } from '../../../auth/auth.service';

@Component({
  selector: 'app-company-detail',
  templateUrl: './company-detail.component.html',
  styleUrls: ['./company-detail.component.scss'],
})
export class CompanyDetailComponent implements OnInit {
  company$!: Observable<Company[]>;
  token: string = '';

  constructor(
    public authService: AuthService,
    private route: ActivatedRoute,
    private router: Router,
    private companyService: CompanyService,
  ) {
  }

  ngOnInit(): void {
    this.company$ = this.route.paramMap.pipe(
      switchMap((params: ParamMap) => this.companyService.getCompanyById(params.get('id')!)),
    );
  }

  deleteCompany(id: string | number) {
    this.authService.currentUser$.pipe(
      switchMap((user) => {
        return this.authService.currentUser$;
      }),
    ).subscribe((user) => {
      this.token = user.token;
      console.log('token:', this.token);
    });

    this.companyService
      .deleteCompany(id, 'Bearer ' + this.token)
      .subscribe(() => this.router.navigate(['..'], { relativeTo: this.route }));
  }
}
