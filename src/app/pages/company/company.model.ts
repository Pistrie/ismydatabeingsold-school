export class Company {
  id: number;
  userId: number;
  name: string;
  website: string;
  foundedDate: Date;
  numberOfEmployees: number;

  constructor(id: number, userId: number, name: string, website: string, foundedDate: Date, numberOfEmployees: number) {
    this.id = id;
    this.userId = userId;
    this.name = name;
    this.website = website;
    this.foundedDate = foundedDate;
    this.numberOfEmployees = numberOfEmployees;
  }
}
