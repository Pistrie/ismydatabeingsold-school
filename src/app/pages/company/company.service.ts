import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Company } from './company.model';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { formatDate } from '@angular/common';

// const httpOptions = {
//   observe: 'body',
//   responseType: 'json',
// };

@Injectable({
  providedIn: 'root',
})
export class CompanyService {
  // private companies: Company[] = [
  //   {
  //     id: 0,
  //     name: 'Meta Platforms',
  //     website: 'about.facebook.com',
  //     foundedDate: new Date('2004-02-01'),
  //     numberOfEmployees: 60654,
  //   },
  //   {
  //     id: 1,
  //     name: 'Apple Inc.',
  //     website: 'apple.com',
  //     foundedDate: new Date('1976-04-01'),
  //     numberOfEmployees: 147000,
  //   },
  //   {
  //     id: 2,
  //     name: 'Amazon',
  //     website: 'amazon.com',
  //     foundedDate: new Date('1994-07-05'),
  //     numberOfEmployees: 1468000,
  //   },
  //   {
  //     id: 3,
  //     name: 'Netflix',
  //     website: 'netflix.com',
  //     foundedDate: new Date('1997-08-29'),
  //     numberOfEmployees: 12135,
  //   },
  //   {
  //     id: 4,
  //     name: 'Google LLC',
  //     website: 'google.com',
  //     foundedDate: new Date('1998-09-04'),
  //     numberOfEmployees: 139995,
  //   },
  // ];

  constructor(private readonly http: HttpClient) {
    // console.log('CompanyService contructor called');
  }

  addCompany(company: Company, token?: any): Observable<Company[]> {
    console.log('addCompany called');

    const endpoint = `${environment.SERVER_API_URL}company`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .post<Company[]>(endpoint, company, httpOptions)
      .pipe(catchError(this.handleError));
  }

  getCompanies(options?: any): Observable<Company[]> {
    console.log('getCompanies called');

    const endpoint = `${environment.SERVER_API_URL}company`;
    console.log(endpoint);

    return this.http
      .get<Company[]>(endpoint, { ...options })
      .pipe(catchError(this.handleError));
  }

  getCompanyById(id: number | string, options?: any): Observable<Company[]> {
    console.log('getCompanyById called');

    const endpoint = `${environment.SERVER_API_URL}company/${id}`;

    return this.http
      .get<Company[]>(endpoint, { ...options })
      .pipe(catchError(this.handleError));
  }

  updateCompany(company: Company, token?: any): Observable<Company> {
    console.log('updateCompany called');

    const endpoint = `${environment.SERVER_API_URL}company/${company.id}`;

    let companyJson: any = {
      name: company.name,
      website: company.website,
      foundedDate: formatDate(company.foundedDate, 'yyyy-MM-dd', 'en-US'),
      numberOfEmployees: company.numberOfEmployees,
    };

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .put<Company[]>(endpoint, companyJson, httpOptions)
      .pipe(catchError(this.handleError));
  }

  deleteCompany(id: string | number, token?: any): Observable<Company[]> {
    console.log('deleteCompany called');

    const endpoint = `${environment.SERVER_API_URL}company/${id}`;

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Authorization: token,
      }),
    };

    return this.http
      .delete<Company[]>(endpoint, httpOptions)
      .pipe(catchError(this.handleError));
  }

  public handleError(error: HttpErrorResponse): Observable<any> {
    console.log(error);

    const errorResponse = {
      type: 'error',
      message: error.error.message || error.message,
    };
    return throwError(errorResponse);
  }
}
