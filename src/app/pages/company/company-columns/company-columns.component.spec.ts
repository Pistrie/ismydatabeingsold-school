import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyColumnsComponent } from './company-columns.component';

describe('CompanyColumnsComponent', () => {
  let component: CompanyColumnsComponent;
  let fixture: ComponentFixture<CompanyColumnsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyColumnsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyColumnsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
