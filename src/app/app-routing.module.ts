import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './core/dashboard/dashboard.component';
import { LayoutComponent } from './core/layout/layout.component';
import { AboutComponent } from './pages/about/about.component';
import {
  ArticleTypeDetailComponent,
} from './pages/article/article-type/article-type-detail/article-type-detail.component';
import { ArticleTypeEditComponent } from './pages/article/article-type/article-type-edit/article-type-edit.component';
import { ArticleTypeListComponent } from './pages/article/article-type/article-type-list/article-type-list.component';
import {
  ProductArticleColumnsComponent,
} from './pages/article/product-article/product-article-columns/product-article-columns.component';
import {
  ProductArticleDetailPlaceholderComponent,
} from './pages/article/product-article/product-article-detail-placeholder/product-article-detail-placeholder.component';
import {
  ProductArticleDetailComponent,
} from './pages/article/product-article/product-article-detail/product-article-detail.component';
import { CompanyColumnsComponent } from './pages/company/company-columns/company-columns.component';
import {
  CompanyDetailPlaceholderComponent,
} from './pages/company/company-detail-placeholder/company-detail-placeholder.component';
import { CompanyDetailComponent } from './pages/company/company-detail/company-detail.component';
import { CompanyEditComponent } from './pages/company/company-edit/company-edit.component';
import { CompanyListComponent } from './pages/company/company-list/company-list.component';
import { ProductColumnsComponent } from './pages/product/product-columns/product-columns.component';
import {
  ProductDetailPlaceholderComponent,
} from './pages/product/product-detail-placeholder/product-detail-placeholder.component';
import { ProductDetailComponent } from './pages/product/product-detail/product-detail.component';
import { ProductEditComponent } from './pages/product/product-edit/product-edit.component';
import { ProductListComponent } from './pages/product/product-list/product-list.component';
import { UserColumnsComponent } from './pages/user/user-columns/user-columns.component';
import { UserDetailPlaceholderComponent } from './pages/user/user-detail-placeholder/user-detail-placeholder.component';
import { UserDetailComponent } from './pages/user/user-detail/user-detail.component';
import { UserEditComponent } from './pages/user/user-edit/user-edit.component';
import { UserListComponent } from './pages/user/user-list/user-list.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { LoggedInAuthGuard } from './auth/auth.guards';
import {
  ArticleTypeDetailPlaceholderComponent,
} from './pages/article/article-type/article-type-detail-placeholder/article-type-detail-placeholder.component';
import {
  ArticleTypeColumnsComponent,
} from './pages/article/article-type/article-type-columns/article-type-columns.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'dashboard' },
      { path: 'register', pathMatch: 'full', component: RegisterComponent },
      { path: 'login', pathMatch: 'full', component: LoginComponent },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'about', component: AboutComponent },
      // users/new moet voor users/:id, omdat new anders als de id wordt gezien.
      // Volgorde is belangrijk in routing.
      {
        path: 'users',
        component: UserColumnsComponent,
        children: [
          { path: '', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: UserDetailPlaceholderComponent },
          { path: 'new', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: UserEditComponent },
          { path: ':id', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: UserDetailComponent },
          { path: ':id/edit', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: UserEditComponent },
        ],
      },
      {
        path: 'companies',
        component: CompanyColumnsComponent,
        children: [
          { path: '', pathMatch: 'full', component: CompanyDetailPlaceholderComponent },
          { path: 'new', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: CompanyEditComponent },
          { path: ':id', pathMatch: 'full', component: CompanyDetailComponent },
          { path: ':id/edit', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: CompanyEditComponent },
        ],
      },
      {
        path: 'products',
        component: ProductColumnsComponent,
        children: [
          { path: '', pathMatch: 'full', component: ProductDetailPlaceholderComponent },
          { path: 'new', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: ProductEditComponent },
          { path: ':id', pathMatch: 'full', component: ProductDetailComponent },
          { path: ':id/edit', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: ProductEditComponent },
        ],
      },
      {
        path: 'articles',
        component: ProductArticleColumnsComponent,
        children: [
          { path: '', pathMatch: 'full', component: ProductArticleDetailPlaceholderComponent },
          { path: 'new', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: ArticleTypeEditComponent },
          { path: ':id', pathMatch: 'full', component: ProductArticleDetailComponent },
          {
            path: ':id/edit',
            pathMatch: 'full',
            canActivate: [LoggedInAuthGuard],
            component: ArticleTypeEditComponent,
          },
        ],
      },
      // the article-list pages exist only in case they are required for the assignment.
      // They cannot be navigated to within the ui itself
      {
        path: 'article-list',
        component: ArticleTypeColumnsComponent,
        children: [
          { path: '', pathMatch: 'full', component: ArticleTypeDetailPlaceholderComponent },
          { path: 'new', pathMatch: 'full', canActivate: [LoggedInAuthGuard], component: ArticleTypeEditComponent },
          { path: ':id', pathMatch: 'full', component: ArticleTypeDetailComponent },
          {
            path: ':id/edit',
            pathMatch: 'full',
            canActivate: [LoggedInAuthGuard],
            component: ArticleTypeEditComponent,
          },
        ],
      },
    ],
  },
  { path: '**', redirectTo: 'dashboard' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
