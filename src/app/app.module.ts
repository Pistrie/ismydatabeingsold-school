import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from 'src/app/core/app/app.component';
import { AlertComponent } from 'src/app/shared/alert/alert.component';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './core/dashboard/dashboard.component';
import { NavbarComponent } from './core/navbar/navbar.component';
import { LayoutComponent } from './core/layout/layout.component';
import { FooterComponent } from './core/footer/footer.component';
import { AboutComponent } from './pages/about/about.component';
import { UsecaseComponent } from './pages/about/usecases/usecase.component';
import { UserListComponent } from './pages/user/user-list/user-list.component';
import { UserDetailComponent } from './pages/user/user-detail/user-detail.component';
import { UserEditComponent } from './pages/user/user-edit/user-edit.component';
import { UserColumnsComponent } from './pages/user/user-columns/user-columns.component';
import { UserDetailPlaceholderComponent } from './pages/user/user-detail-placeholder/user-detail-placeholder.component';
import { CompanyListComponent } from './pages/company/company-list/company-list.component';
import { CompanyDetailComponent } from './pages/company/company-detail/company-detail.component';
import {
  CompanyDetailPlaceholderComponent,
} from './pages/company/company-detail-placeholder/company-detail-placeholder.component';
import { CompanyEditComponent } from './pages/company/company-edit/company-edit.component';
import { CompanyColumnsComponent } from './pages/company/company-columns/company-columns.component';
import { EnumToArrayPipe } from './enum-to-array.pipe';
import { ProductColumnsComponent } from './pages/product/product-columns/product-columns.component';
import { ProductDetailComponent } from './pages/product/product-detail/product-detail.component';
import {
  ProductDetailPlaceholderComponent,
} from './pages/product/product-detail-placeholder/product-detail-placeholder.component';
import { ProductEditComponent } from './pages/product/product-edit/product-edit.component';
import { ProductListComponent } from './pages/product/product-list/product-list.component';
import {
  ProductArticleDetailComponent,
} from './pages/article/product-article/product-article-detail/product-article-detail.component';
import {
  ProductArticleDetailPlaceholderComponent,
} from './pages/article/product-article/product-article-detail-placeholder/product-article-detail-placeholder.component';
import {
  ArticleTypeDetailComponent,
} from './pages/article/article-type/article-type-detail/article-type-detail.component';
import {
  ProductArticleColumnsComponent,
} from './pages/article/product-article/product-article-columns/product-article-columns.component';
import {
  ArticleTypeColumnsComponent,
} from './pages/article/article-type/article-type-columns/article-type-columns.component';
import {
  ArticleTypeDetailPlaceholderComponent,
} from './pages/article/article-type/article-type-detail-placeholder/article-type-detail-placeholder.component';
import { ArticleTypeEditComponent } from './pages/article/article-type/article-type-edit/article-type-edit.component';
import { ArticleTypeListComponent } from './pages/article/article-type/article-type-list/article-type-list.component';
import { RegisterComponent } from './auth/register/register.component';
import { LoginComponent } from './auth/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { LoggedInAuthGuard } from './auth/auth.guards';
import { EmailValidatorDirective } from './validators/email-validator.directive';

@NgModule({
  declarations: [
    AppComponent,
    AlertComponent,
    NavbarComponent,
    LayoutComponent,
    DashboardComponent,
    FooterComponent,
    AboutComponent,
    UsecaseComponent,
    UserListComponent,
    UserDetailComponent,
    UserEditComponent,
    UserColumnsComponent,
    UserDetailPlaceholderComponent,
    CompanyListComponent,
    CompanyDetailComponent,
    CompanyDetailPlaceholderComponent,
    CompanyEditComponent,
    CompanyColumnsComponent,
    EnumToArrayPipe,
    ProductColumnsComponent,
    ProductDetailComponent,
    ProductDetailPlaceholderComponent,
    ProductEditComponent,
    ProductListComponent,
    ProductArticleDetailComponent,
    ProductArticleDetailPlaceholderComponent,
    ArticleTypeDetailComponent,
    ProductArticleColumnsComponent,
    ArticleTypeColumnsComponent,
    ArticleTypeDetailPlaceholderComponent,
    ArticleTypeEditComponent,
    ArticleTypeListComponent,
    RegisterComponent,
    LoginComponent,
    EmailValidatorDirective,
  ],
  imports: [BrowserModule, RouterModule, NgbModule, AppRoutingModule, FormsModule, HttpClientModule],
  providers: [LoggedInAuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {
}
