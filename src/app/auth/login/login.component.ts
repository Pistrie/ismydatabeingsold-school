import { Component, OnDestroy, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { User } from '../../pages/user/user.model';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../auth.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {
  subscription: any;
  submitted = false;
  user: any = { email: '', password: '' };

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit(): void {
    this.subscription = this.authService
      .getUserFromLocalStorage()
      .subscribe((user: User) => {
        if (user) {
          console.log('User already logged in > to dashboard');
          this.router.navigate(['/']);
        }
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  onSubmit(): any {
    console.log('onSubmit', this.user);

    this.submitted = true;
    const email = this.user.email;
    const password = this.user.password;
    this.authService
      .login(email, password)
      .subscribe((user) => {
        if (user) {
          console.log('Logged in');
          this.router.navigate(['/']);
        }
        this.submitted = false;
      });
  }

  // validEmail(control: FormControl): { [p: string]: boolean } {
  //   const email = control.value;
  //   const regexp = new RegExp(
  //     '^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$',
  //   );
  //   if (!regexp.test(email)) {
  //     return { email: false };
  //   } else {
  //     return { email: true };
  //   }
  // }
  //
  // validPassword(control: FormControl): { [p: string]: boolean } {
  //   const password = control.value;
  //   const regexp = new RegExp('^[a-zA-Z]([a-zA-Z0-9]){2,14}');
  //   const test = regexp.test(password);
  //   if (!regexp.test(password)) {
  //     return { password: false };
  //   } else {
  //     return { password: true };
  //   }
  // }
}
