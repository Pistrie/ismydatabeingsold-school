import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { User, UserRole } from '../../pages/user/user.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['../auth.scss'],
})
export class RegisterComponent {
  subscription: any;
  // TODO add all user properties
  user: any = { firstName: '', lastName: '', emailAddress: '', dateOfBirth: '', role: '', password: '' };

  constructor(private authService: AuthService, private router: Router) {
  }

  onSubmit(): void {
    console.log('onSubmit', this.user);

    const firstName = this.user.firstName;
    const lastName = this.user.lastName;
    const emailAddress = this.user.emailAddress;
    const dateOfBirth = this.user.dateOfBirth;
    const role = UserRole.user;
    const password = this.user.password;
    const registerUser: User = new User(firstName, lastName, emailAddress, dateOfBirth, role, password);
    this.authService
      .register(registerUser)
      .subscribe((user) => {
        if (user) {
          console.log('user = ', user);
          this.router.navigate(['/']);
        }
      });
  }
}
