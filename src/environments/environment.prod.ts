const packagejson = require('../../package.json');

export const environment = {
  production: true,

  // Fill in your own online server API url here
  SERVER_API_URL: 'https://imdbs-db.pistrie.duckdns.org/api/',

  version: packagejson.version,
};
